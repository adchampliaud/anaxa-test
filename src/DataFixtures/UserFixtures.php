<?php declare(strict_types = 1);
/**
 * Created by PhpStorm.
 * User: nicolas
 * Date: 12/07/18
 * Time: 17:33
 */

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    public function __construct(
        private readonly UserPasswordEncoderInterface $passwordEncoder,
    ) {
    }
    /**
     * Load data fixtures with the passed EntityManager
     */
    public function load(ObjectManager $manager): void
    {
        $investor = new User(
            'John',
            'Doe',
            'john@local.com',
            'john',
            ['ROLE_USER'],
        );
        $password = $this->passwordEncoder->encodePassword($investor, $investor->getPlainPassword());
        $investor->setPassword($password);
        $manager->persist($investor);

        $admin = new User(
            'admin',
            'anaxago',
            'admin@local.com',
            'admin',
            ['ROLE_ADMIN'],
        );
        $password = $this->passwordEncoder->encodePassword($admin, $admin->getPlainPassword());
        $admin->setPassword($password);
        $manager->persist($admin);

        $manager->flush();
    }
}
