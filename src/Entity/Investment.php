<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="investment")
 */
final class Investment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="financedProjects")
     * @ORM\JoinColumn(nullable=false)
     */
    private UserInterface $investor;

    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="investors")
     * @ORM\JoinColumn(nullable=false)
     */
    private Project $project;

    /**
     * @ORM\Column(type="integer")
     */
    private int $amount;

    public function __construct(
        UserInterface $investor,
        Project $project,
        int $amount,
    ) {
        $this->investor = $investor;
        $this->project = $project;
        $this->amount = $amount;
    }

    public function updateAmount(int $amount): void
    {
        $this->amount += $amount;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getProject(): Project
    {
        return $this->project;
    }
}