<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Project
 * @ORM\Entity
 * @ORM\Table(name="project")
 */
class Project
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $slug;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $title;

    /**
     * @ORM\Column(type="text")
     */
    private string $description;

    /**
     * @ORM\Column(type="integer")
     */
    private int $requiredInvestment;

    /**
     * @ORM\OneToMany(targetEntity="Investment", mappedBy="project", fetch="EXTRA_LAZY")
     */
    private Collection $investors;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isInvested;

    public function __construct(
        string $title,
        string $description,
        string $slug,
        int $requiredInvestment,
    ) {
        $this->title = $title;
        $this->description = $description;
        $this->slug = $slug;
        $this->requiredInvestment = $requiredInvestment;
        $this->investors = new ArrayCollection();
        $this->isInvested = false;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function isInvested(): bool
    {
        return $this->isInvested;
    }

    public function getRequiredInvestment(): int
    {
        return $this->requiredInvestment;
    }

    public function hasInvested(): self
    {
        $this->isInvested = true;

        return $this;
    }
}

