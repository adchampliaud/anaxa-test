<?php

declare(strict_types=1);

namespace App\Service;

use App\Repository\ProjectRepositoryInterface;

final class ProjectService implements ProjectServiceInterface
{
    public function __construct(
        private readonly ProjectRepositoryInterface $projectRepository,
    ) {
    }

    public function getProjects(): array
    {
        return $this->projectRepository->getAll();
    }
}