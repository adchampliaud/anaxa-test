<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Investment;

interface InvestmentServiceInterface
{
    public function invest(int $projectId, int $amount): void;

    /**
     * @return Investment[]
     */
    public function getInvestmentsByConnectedUser(): array;
}