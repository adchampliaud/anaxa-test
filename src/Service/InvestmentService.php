<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Project;
use App\Entity\Investment;
use App\Entity\User;
use App\Repository\InvestmentRepositoryInterface;
use App\Repository\ProjectRepositoryInterface;
use DomainException;
use Symfony\Component\Security\Core\Security;

final class InvestmentService implements InvestmentServiceInterface
{
    public function __construct(
        private readonly Security $security,
        private readonly InvestmentRepositoryInterface $investmentRepository,
        private readonly ProjectRepositoryInterface $projectRepository,
    ) {
    }

    public function getInvestmentsByConnectedUser(): array
    {
        $user = $this->security->getUser();
        if (!$user instanceof User) {
            throw new DomainException(
                sprintf(
                    'User %s is not instance of %s',
                    $user->getUserIdentifier(),
                    User::class),
            );
        }

        return $user->getFinancedProjects();
    }

    public function invest(int $projectId, int $amount): void
    {
        $user = $this->security->getUser();
        if ($user === null) {
            throw new DomainException('User is null');
        }
        $project = $this->projectRepository->getById($projectId);
        if ($project === null) {
            throw new DomainException(sprintf('Project not found with id %s', $projectId));
        }

        $investment = $this->investmentRepository->getByUserAndProject($user, $project);

        if ($investment === null) {
            $this->investmentRepository->save(
                new Investment($user, $project, $amount),
            );
            $this->updateProject($project);
            return;
        }

        $investment->updateAmount($amount);
        $this->investmentRepository->save($investment);
        $this->updateProject($project);
    }

    private function updateProject(Project $project): void
    {
        if ($project->isInvested()) {
            return;
        }

        $projectFunders = $this->investmentRepository->getByProject($project);

        $investment = 0;
        foreach ($projectFunders as $projectFunder) {
            $investment += $projectFunder->getAmount();
            if ($investment >= $project->getRequiredInvestment()) {
                $project->hasInvested();
                $this->projectRepository->save($project);
            }
        }
    }
}