<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Project;

interface ProjectServiceInterface
{
    /**
     * @return Project[]
     */
    public function getProjects(): array;
}