<?php declare(strict_types = 1);

namespace App\Controller;

use App\Entity\Investment;
use App\Entity\Project;
use App\Entity\User;
use App\Service\InvestmentServiceInterface;
use App\Service\ProjectServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use DomainException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function __construct(
        private readonly InvestmentServiceInterface $investmentService,
        private readonly ProjectServiceInterface $projectService,
    ) {
    }

    public function index(EntityManagerInterface $entityManager): Response
    {
        $projects = $entityManager->getRepository(Project::class)->findAll();

        return $this->render('Default/index.html.twig', ['projects' => $projects]);
    }

    public function stats(EntityManagerInterface $entityManager): Response
    {
        $users = $entityManager->getRepository(User::class)->findAll();
        $projects = $entityManager->getRepository(Project::class)->findAll();

        $accessLogs = file_get_contents('/var/log/apache2/anaxago-starter/access.log');
        $accessStats = [];
        foreach (explode("\n", $accessLogs) as $accessLog) {
            if (!$accessLog) {
                continue;
            }
            $data = explode(' ', $accessLog)[3];
            $start = null;
            $end = null;
            foreach(str_split($data) as $index => $char) {
                if ($char === '/') {
                    if ($start !== null) {
                        $end = $index;
                    }
                    if ($start === null) {
                        $start = $index;
                    }
                }
            }
            $start -=- 1;
            $data = substr($data, $start, $end-$start);
            if (in_array($data, array_keys($accessStats))) {
                $accessStats[$data] = ++$accessStats[$data];
            } else {
                $accessStats[$data] = 1;
            }
        }
        dump($accessStats);

        return $this->render('Default/stats.html.twig', ['users' => count($users), 'projects' => $projects, 'accessStats' => $accessStats]);
    }

    public function project(EntityManagerInterface $entityManager, int $id): Response
    {
        if (!$this->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }
        $project = $entityManager->getRepository(Project::class)->find($id);

        $form = $this->createFormBuilder(
            null,
            [
                'action' => $this->generateUrl('anaxago_create_investment', ['projectId' => $project->getId()])
            ]
        )->add('amount', NumberType::class, ['label' => 'Montant à investir :'])
            ->add('submit', SubmitType::class, ['label' => 'Continuer'])
            ->getForm();

        return $this->render('Default/project.html.twig', ['project' => $project, 'form' => $form->createView()]);
    }

    public function createInvestment(Request $request): Response
    {
        if ($this->isGranted('ROLE__USER')) {
            throw $this->createAccessDeniedException();
        }

        $amount = $request->request->get('form')['amount'];
        $this->investmentService->invest((int) $request->query->get('projectId'), (int) $amount);

        return $this->render('Default/createInvestment.html.twig', ['amount' => $amount]);
    }

    public function invest(Request $request): Response
    {
        if (!$this->isGranted('ROLE_USER')) {
            throw $this->createAccessDeniedException();
        }

        $requestBody = json_decode((string) $request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        if (!array_key_exists('projectId', $requestBody)) {
            throw new DomainException('Request investment need projectId');
        }

        $this->investmentService->invest($requestBody['projectId'], $requestBody['amount']);
        return new JsonResponse(['success']);
    }

    public function getProjects(): JsonResponse
    {
        return new JsonResponse(array_map(
            fn (Project $project) => ['title' => $project->getTitle(), 'isFunding' => $project->isInvested()],
            $this->projectService->getProjects(),
        ));
    }

    public function investmentsByUser(): JsonResponse
    {
        return new JsonResponse(array_map(
            fn (Investment $investment) => ['title' => $investment->getProject()->getTitle(), 'amount' => $investment->getAmount()],
            $this->investmentService->getInvestmentsByConnectedUser(),
        ));
    }
}
