<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Project;

interface ProjectRepositoryInterface
{
    public function getById(int $projectId): ?Project;

    public function save(Project $project): void;

    public function getAll(): array;
}