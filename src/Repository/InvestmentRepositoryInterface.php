<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Project;
use App\Entity\Investment;
use Symfony\Component\Security\Core\User\UserInterface;

interface InvestmentRepositoryInterface
{
    public function getByUserAndProject(UserInterface $investor, Project $project): ?Investment;

    /**
     * @return Investment[]
     */
    public function getByProject(Project $project): array;

    public function save(Investment $investment): void;
}