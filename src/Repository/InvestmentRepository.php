<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Project;
use App\Entity\Investment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final class InvestmentRepository implements InvestmentRepositoryInterface
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    public function getByUserAndProject(UserInterface $investor, Project $project): ?Investment
    {
        return $this->entityManager->createQueryBuilder()
            ->select('i')
            ->from(Investment::class, 'i')
            ->where('i.investor = :investor')
            ->setParameter('investor', $investor)
            ->andWhere('i.project = :project')
            ->setParameter('project', $project)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getByProject(Project $project): array
    {
        return $this->entityManager->createQueryBuilder()
            ->select('i')
            ->from(Investment::class, 'i')
            ->where('i.project = :project')
            ->setParameter('project', $project)
            ->getQuery()
            ->getResult();
    }

    public function save(Investment $investment): void
    {
        $this->entityManager->persist($investment);
        $this->entityManager->flush();
    }
}