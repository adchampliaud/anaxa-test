<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Project;
use Doctrine\ORM\EntityManagerInterface;

final class ProjectRepository implements ProjectRepositoryInterface
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
    ) {
    }

    public function getById(int $projectId): ?Project
    {
        return $this->entityManager->find(Project::class, $projectId);
    }

    public function save(Project $project): void
    {
        $this->entityManager->persist($project);
        $this->entityManager->flush();;
    }

    public function getAll(): array
    {
        return $this->entityManager->getRepository(Project::class)->findAll();
    }
}
