<?php

declare(strict_types=1);

namespace App\Tests;

use App\Entity\Project;
use App\Repository\InvestmentRepositoryInterface;
use App\Repository\ProjectRepositoryInterface;
use App\Service\InvestmentServiceInterface;
use DomainException;

final class InvestmentServiceIntegrationTest extends AbstractIntegrationTest
{
    private readonly InvestmentServiceInterface $investmentService;
    private readonly ProjectRepositoryInterface $projectRepository;
    private readonly InvestmentRepositoryInterface $investmentRepository;

    public function setUp(): void
    {
        $this->investmentService = $this->getService(InvestmentServiceInterface::class);
        $this->projectRepository = $this->getService(ProjectRepositoryInterface::class);
        $this->investmentRepository = $this->getService(InvestmentRepositoryInterface::class);
    }

    /** @test */
    public function it_should_throw_exception_when_project_not_found1(): void
    {
        $projectId = 111111;
        self::expectException(DomainException::class);
        self::expectExceptionMessage('User is null');
        $this->investmentService->invest($projectId, 1000);
    }

    /** @test */
    public function it_should_throw_exception_when_project_not_found(): void
    {
        $this->authenticateBasicUser();
        $projectId = 111111;
        self::expectException(DomainException::class);
        self::expectExceptionMessage(sprintf('Project not found with id %s', $projectId));
        $this->investmentService->invest($projectId, 1000);
    }

    /** @test */
    public function it_should_invest_in_project(): void
    {
        $user = $this->authenticateBasicUser();
        /** @var Project $project */
        $project = current($this->projectRepository->getAll());
        $investment = $this->investmentRepository->getByUserAndProject($user, $project);
        $amount = 100;
        $expectedAmount = $investment !== null ? $amount + $investment->getAmount() : $amount;
        $this->investmentService->invest($project->getId(), $amount);
        $expectedInvestment = $this->investmentRepository->getByUserAndProject($user, $project);
        self::assertEquals($expectedInvestment->getAmount(), $expectedAmount);
    }

    /** @test */
    public function it_should_reach_required_investment(): void
    {
        $this->authenticateBasicUser();
        /** @var Project $project */
        $project = current($this->projectRepository->getAll());
        $this->investmentService->invest($project->getId(), $project->getRequiredInvestment());

        $expectedInvestment = $this->projectRepository->getById($project->getId());
        self::assertTrue($expectedInvestment->isInvested());
    }
}