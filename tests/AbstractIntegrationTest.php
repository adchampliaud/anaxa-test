<?php

declare(strict_types=1);

namespace App\Tests;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class AbstractIntegrationTest extends KernelTestCase
{
    public function getService(string $className): object
    {
        return self::getContainer()->get($className);
    }

    public function authenticateBasicUser(): UserInterface
    {
        $user = new User('first_name', 'last_name', 'basic@email.com', 'basic_password', ['ROLE_USER']);
        $user->setPassword('basic_password');


        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        self::getContainer()->get('security.token_storage')->setToken($token);
        self::getContainer()->get('session')->set('_security_main', serialize($token));

        $entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $entityManager->persist($user);
        $entityManager->flush();

        return $user;
    }
}